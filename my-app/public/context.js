import React, { createContext, useContext, useState } from 'react';

const context = createContext();

export function contextProvider({ children }) {
  const [equipoAcronimo, setEquipoAcronimo] = useState('');
  const [jugadoresParametro, setJugadoresParametro] = useState('');
  const [imagenEquipo, setImagenEquipo] = useState('');

  return (
    <context.Provider
      value={{
        equipoAcronimo,
        jugadoresParametro,
        imagenEquipo,
        setEquipoAcronimo,
        setJugadoresParametro,
        setImagenEquipo,
      }}
    >
      {children}
    </context.Provider>
  );
}

export function useMiContexto() {
  return useContext(context);
}